These are some split packages to build a complete Pure system from git sources.

The build process is described in more detail in the pure-complete/PKGBUILD file. In brief:

- Install llvm35, pure and pure-gen from the AUR, in that order.

- Run makepkg in pure-bootstrap-git and install the resulting packages. This provides all the dependencies needed for a full build.

- Run makepkg in pure-complete-git to get a full set of packages.

NOTES:

- If you have trouble installing llvm35 from the AUR, try to build the package manually with makepkg in the llvm35 directory. Also, be patient. Building LLVM (and clang) can take a long time!

- If you have trouble installing pure-gen from the AUR, try the alternative PKGBUILD in the pure-gen-prebuilt directory. This includes a precompiled dump-ast executable and can be built without having any Haskell dependencies installed.

Good luck!

Albert Gräf <aggraef@gmail.com>
